var fs = require('fs');
var bunyan = require('bunyan');


function randint(n) {
    return Math.floor(Math.random() * n);
}

function randchoice(array) {
    return array[randint(array.length)];
}

var words = ['hello', 'world', 'random', 'words'];
var levels = ['trace', 'debug', 'info', 'warn', 'error', 'fatal'];
var timeout;

var log = bunyan.createLogger({
    name: 'thelog',
    stream: process.stdout,
    level: "debug"
});

// We're logging to stdout. Let's exit gracefully on EPIPE. E.g. if piped
// to `head` which will close after N lines.
process.stdout.on('error', function (err) {
    if (err.code === 'EPIPE') {
        process.exit(0);
    }
});

function logOne() {
    var level = randchoice(levels);
    var msg = [randchoice(words), randchoice(words)].join(' ');
    var delay = randint(300);
    
    log[level]({'word': randchoice(words), 'delay': delay}, msg);
    timeout = setTimeout(logOne, delay);
}

log.info('hi, this is the start');
timeout = setTimeout(logOne, 1000);